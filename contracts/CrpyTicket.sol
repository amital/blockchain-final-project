pragma solidity >=0.4.22 <0.6.0;

contract CrypTicket{
    
    uint32 private total_tickets;
    uint32 private pending_tickets;
    
    address private creator;

    mapping(address => uint32) owners;
    mapping(address => uint32) pendingOwners;
    mapping(address => uint32) pendingTransfers;
    mapping(address => address) pendingTransferAddresses;
    
    modifier onlyCreator{
        require(msg.sender == creator, "Can only be called by the creator of the ticket");
        _;
    }
    
    constructor(uint32 number_of_tickets) public {
        creator = msg.sender;
        total_tickets = number_of_tickets;
    }
    
    
    function getTotalAvailable() public view returns (uint32){
        assert(total_tickets >= pending_tickets);
        return total_tickets - pending_tickets;
    }
    
    function getAccountTickets(address account_id) public view returns (uint32){
        return owners[account_id];
    }
    
    
    /* While waiting for payment to clear, 'reserve' a ticket */
    // DoS attack?
    function setPending(address account_id, uint32 amount) public onlyCreator returns (bool){
        require(pendingOwners[account_id] == uint32(0), "You already have tickets pending"); // Prevent setting a bunch as pending
        if ((total_tickets - pending_tickets) >= amount && amount > uint32(0)){
            pendingOwners[account_id] = amount;
            pending_tickets += amount;
            return true;
        }else{
            return false;
        }
    }
    
    function getPending(address account_id) public view returns (uint32){
        return pendingOwners[account_id];
    }
    
    function unsetPending(address account_id) public onlyCreator{
        if (pendingOwners[account_id] > 0 && pending_tickets >= pendingOwners[account_id]){
            pending_tickets -= pendingOwners[account_id];
            pendingOwners[account_id] = 0;
        }
    }
    
    /* Give pending buyer a ticket */
    function buyTicket(address account_id, uint32 amount) public onlyCreator {
        require(pendingOwners[account_id] == amount, "Buying more tickets than are pending");
        
        // These should be impossible
        assert(pending_tickets >= amount);
        assert(total_tickets >= amount);
        
        owners[account_id] += amount;
        total_tickets -= amount;
        pendingOwners[account_id] = 0;
        pending_tickets -= amount;
    }
    
    // TODO make more scalable? Nested mapping?
    /* Reserve tickets for transfer */
    function setTransferPending(address account_id, address new_owner, uint32 amount) public onlyCreator{
        require(owners[account_id] >= amount, "Cannot transfer more tickets than owned");
        require(pendingTransferAddresses[account_id] == address(0), "Must complete existing transfers first");
        require(pendingTransfers[account_id] == 0);
        pendingTransferAddresses[account_id] = new_owner;
        pendingTransfers[account_id] = amount;
    }
    
    function unsetTransferPending(address account_id, address new_owner) public onlyCreator {
        require(pendingTransferAddresses[account_id] == new_owner);
        pendingTransfers[account_id] = 0;
        pendingTransferAddresses[account_id] = address(0);
    }
    
    /* Transfer ticket after payment is verified */
    function transferTicket(address account_id, address new_owner) public onlyCreator {
        require(pendingTransferAddresses[account_id] == new_owner);
        owners[new_owner] += pendingTransfers[account_id];
        owners[account_id] -= pendingTransfers[account_id];
        pendingTransfers[account_id] = 0;
        pendingTransferAddresses[account_id] = address(0);
    }
    
}