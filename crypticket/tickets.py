from flask import (
	Blueprint, request, render_template,
	g, redirect, url_for, current_app
)
import time
from crypticket.db import get_db
from crypticket.users import login_required
from web3 import Web3, eth, HTTPProvider
import stripe
import requests
from . import contract_abi

w3 = Web3(HTTPProvider('http://127.0.0.1:8545'))

contract = None

bp = Blueprint('tickets', __name__)


def init_contract(app):
	global contract
	contract = w3.eth.contract(address = app.config["CONTRACT_ADDR"], abi = contract_abi.abi)
	stripe.api_key = app.config["STRIPE_SK"]
	print("Connnected to contract")


def send_sign_transaction(txn):
	signed_txn = w3.eth.account.signTransaction(txn, private_key=current_app.config["ETH_SK"])
	txn_hash = w3.eth.sendRawTransaction(signed_txn.rawTransaction)
	txn_receipt = None
	count = 0
	while txn_receipt is None and (count < 30):
		count = count + 1
		txn_receipt = w3.eth.getTransactionReceipt(txn_hash)
		print(txn_receipt)
		time.sleep(0.5)
	return txn_receipt

def release_pending():
	txn = contract.functions.unsetPending(g.user["address"]).buildTransaction({
		'gasPrice': w3.toWei('40', 'gwei'),
		'nonce': w3.eth.getTransactionCount(current_app.config["ETH_WALLET"]),
	})
	signed_txn = w3.eth.account.signTransaction(txn, private_key=current_app.config["ETH_SK"])
	txn_hash = w3.eth.sendRawTransaction(signed_txn.rawTransaction)
	txn_receipt = None
	count = 0
	while txn_receipt is None and (count < 30):
		count += 1
		txn_receipt = w3.eth.getTransactionReceipt(txn_hash)
		time.sleep(0.25)
	return txn_receipt

def release_transfer(username):
	txn = contract.functions.unsetTransferPending(g.user["address"], username).buildTransaction({
		'gasPrice': w3.toWei('40', 'gwei'),
		'nonce': w3.eth.getTransactionCount(current_app.config["ETH_WALLET"]),
	})
	signed_txn = w3.eth.account.signTransaction(txn, private_key=current_app.config["ETH_SK"])
	txn_hash = w3.eth.sendRawTransaction(signed_txn.rawTransaction)
	txn_receipt = None
	count = 0
	while txn_receipt is None and (count < 30):
		count += 1
		txn_receipt = w3.eth.getTransactionReceipt(txn_hash)
		time.sleep(0.25)
	return txn_receipt

@bp.route('/')
@login_required
def index(message = ''):

	if request.args.get('message'):
		message = request.args.get('message')
	num_tickets = contract.functions.getAccountTickets(g.user["address"]).call()
	return render_template('index.html', tickets=num_tickets, message = message)

@bp.route('/buy', methods=['GET', 'POST'])
@login_required
def buy():

	if request.method == "GET":
		tokens_available = contract.functions.getTotalAvailable().call()
		return render_template('buy.html', error = '', available=tokens_available)

	# Get input
	token = request.form["stripeToken"]
	num_tickets = int(request.form["num_tickets"])

	# Reserve tickets
	txn = contract.functions.setPending(g.user["address"], num_tickets).buildTransaction({
		'gasPrice': w3.toWei('40', 'gwei'),
		'nonce': w3.eth.getTransactionCount(current_app.config["ETH_WALLET"]),
	})
	txn_receipt = send_sign_transaction(txn)

	if txn_receipt is None:
		release_pending()
		tokens_available = contract.functions.getTotalAvailable().call()
		return render_template('buy.html', error = 'Ticket reservation timed out (you have not been charged)', available=tokens_available)
	
	# Check payment
	try:
		charge = stripe.Charge.create(
			amount = num_tickets*100,
			currency='usd',
			description='Ticket purchase',
			source=token
		)
	except stripe.error.CardError as e:
		release_pending()
		
		err = e.json_body.get('error', {}).get('message')
		tokens_available = contract.functions.getTotalAvailable().call()
		return render_template('buy.html', error = error, available=tokens_available)
	
	txn_receipt = None
	error = None
	try:
		txn = contract.functions.buyTicket(g.user["address"], num_tickets).buildTransaction({
			'gasPrice': w3.toWei('40', 'gwei'),
			'nonce': w3.eth.getTransactionCount(current_app.config["ETH_WALLET"]),
		})
		txn_receipt = send_sign_transaction(txn)
	except Exception as e:
		error = e
	if txn_receipt is None or error is not None:
		tokens_available = contract.functions.getTotalAvailable().call()
		return render_template('buy.html', error = 'Failed to acquire tickets', available=tokens_available)
	
	return redirect(url_for('index', message='Successfully purchased tickets'))

@bp.route('/transfer', methods=['GET', 'POST'])
@login_required
def transfer():

	num_tickets = contract.functions.getAccountTickets(g.user["address"]).call()

	if request.method == 'GET':
		return render_template('transfer.html', error = '', tickets=num_tickets)
		
	username = request.form['username']
	num_to_transfer = int(request.form['num_tickets'])
	token = request.form["stripeToken"]
	
	if num_to_transfer > num_tickets:
		return render_template('transfer.html', error='Cannot transfer more than owned', tickets=num_tickets)
	
	# Lookup transfer
	db = get_db()
	user_to_recv = get_db().execute(
			'SELECT * FROM Users WHERE username = ?', (username,)
	).fetchone()
	
	if user_to_recv is None:
		return render_template('transfer.html', error="User '{}' not found".format(username), tickets = num_tickets)

	# set pending
	try:
		txn = contract.functions.setTransferPending(g.user["address"], user_to_recv["address"], num_to_transfer).buildTransaction({
			'gasPrice': w3.toWei('40', 'gwei'),
			'nonce': w3.eth.getTransactionCount(current_app.config["ETH_WALLET"]),
		})
		txn_receipt = send_sign_transaction(txn)
	except:
		release_transfer(user_to_recv["address"])
		txn = contract.functions.setTransferPending(g.user["address"], user_to_recv["address"], num_to_transfer).buildTransaction({
			'gasPrice': w3.toWei('40', 'gwei'),
			'nonce': w3.eth.getTransactionCount(current_app.config["ETH_WALLET"]),
		})
		txn_receipt = send_sign_transaction(txn)
	
	# Get payment
	try:
		charge = stripe.Charge.create(
			amount = 500,
			currency='usd',
			description='Ticket purchase',
			source=token
		)
	except stripe.error.CardError as e:
		release_transfer(user_to_recv["address"])
		err = e.json_body.get('error', {}).get('message')
		num_tickets = contract.functions.getAccountTickets(g.user["address"]).call()
		return render_template('transfer.html', error = err, tickets=num_tickets)
		
	txn_receipt = None
	error = None
	try:
		txn = contract.functions.transferTicket(g.user["address"], user_to_recv["address"]).buildTransaction({
			'gasPrice': w3.toWei('40', 'gwei'),
			'nonce': w3.eth.getTransactionCount(current_app.config["ETH_WALLET"]),
		})
		txn_receipt = send_sign_transaction(txn)
	except Exception as e:
		error = e
		release_transfer(user_to_recv["address"])
	if txn_receipt is None or error is not None:
		num_tickets = contract.functions.getAccountTickets(g.user["address"]).call()
		return render_template('transfer.html', error = error, tickets=num_tickets)
	
	return redirect(url_for('index', message='Transfer successful'))

@bp.route('/redeem', methods=['GET'])
@login_required
def redeem():
	return render_template('redeem.html')
	
@bp.route('/count', methods=['POST'])
def count():
	address = request.form["address"]
	num_tickets = contract.functions.getAccountTickets(address).call()
	return str(num_tickets)

	