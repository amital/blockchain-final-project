from flask import (
	Blueprint, request, render_template,
	g, redirect, url_for, session, current_app
)
import functools
from werkzeug.security import check_password_hash, generate_password_hash
import requests
from crypticket.db import get_db
from web3 import Web3 # Convert to address they like

bp = Blueprint('users', __name__)

@bp.before_app_request
def load_user():
	id = session.get('user')
	if id is None:
		g.user = None
	else:
		g.user = get_db().execute(
			'SELECT * FROM Users WHERE id = ?', (id,)
		).fetchone()

def login_required(view):
    @functools.wraps(view)
    def wrapped_view(**kwargs):
        if g.user is None:
            return redirect(url_for('users.login'))

        return view(**kwargs)

    return wrapped_view


@bp.route('/register', methods=['GET', 'POST'])
def register():
	if request.method == 'GET':
		return render_template('register.html')

	username = request.form["username"]
	password = request.form["password"]
	password_check = request.form["password_check"]
		
	if not username or not password or not password_check:
		return render_template('register.html', error="Missing credentials")
	
	if password != password_check:
		return render_template('register.html', error="Passwords do not match")

	
	db = get_db()
	if db.execute(
		'SELECT id FROM Users WHERE username = ?', (username,)
	).fetchone() is not None:
		return render_template('register.html', error = "User {} is already registered".format(username))
	
	address = '0x' + requests.post('https://api.blockcypher.com/v1/eth/main/addrs?token=' + current_app.config["BLOCKCYPHER_TOKEN"]).json()["address"]
	address = Web3.toChecksumAddress(address)
	
	db.execute(
		'INSERT INTO Users (username, password, address) VALUES (?, ?, ?)',
		(username, generate_password_hash(password), address)
	)
	db.commit()
	
	user = db.execute(
            'SELECT * FROM Users WHERE username = ?', (username,)
    ).fetchone()
	
	session.clear()
	session["user"] = user["id"]
	
	return redirect(url_for('index'))
	


@bp.route('/login', methods=['GET', 'POST'])
def login():

	# Switch to redirect for all?
	if request.method == 'GET':
		if g.user is not None:
			return redirect(url_for('index'))
		return render_template('login.html', error='')

	username = request.form["username"]
	print(request.form)
	password = request.form["password"]
		
	if not username or not password:
		return render_template('login.html', error="Missing credentials")
		
	user = get_db().execute(
            'SELECT * FROM Users WHERE username = ?', (username,)
    ).fetchone()
		
	if user is None:
		return render_template('login.html', error="User does not exist")
	elif not check_password_hash(user["password"], password):
		return render_template('login.html', error="Incorrect password")
		
	session.clear()
	session["user"] = user["id"]
	return redirect(url_for('index'))


@bp.route('/logout')
def logout():
	session.clear()
	return redirect(url_for('users.login'))