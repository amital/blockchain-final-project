import sqlite3
from flask import current_app, g

def get_db():
	if 'db' not in g:
		g.db = sqlite3.connect(
			current_app.config['DATABASE'],
			detect_types=sqlite3.PARSE_DECLTYPES
		)
		g.db.row_factory = sqlite3.Row
	return g.db

def init_db(app):
	db = sqlite3.connect(
			app.config['DATABASE'],
			detect_types=sqlite3.PARSE_DECLTYPES
		)
	db.row_factory = sqlite3.Row
	with app.open_resource('schema.sql') as schema_file:
		db.executescript(schema_file.read().decode('utf8'))
	print("Initialized database")
	
def close_db(e=None):
	db = g.pop('db', None)
	if db is not None:
		db.close()
		
def init_app(app):
	#init_db(app)
	app.teardown_appcontext(close_db)

