#!/usr/bin/python
from flask import Flask
import os
from . import db, users, tickets



def create_app():
	
	app = Flask(__name__, instance_relative_config=True)
	
	app.config.from_mapping(
		SECRET_KEY='dev',
		DATABASE=os.path.join(app.instance_path, 'crypticket.sqlite'),
		STRIPE_SK='sk_test_MLpQhiPwNpBy5XEzYWVXsONs00OwK1zoZ7',
		BLOCKCYPHER_TOKEN="d98d31c0f3074cac92374f8ad1e2d6d4",
		ETH_SK='0x57dccc5e75c14980dbdea2a2e00a67467c1f2fde5b6cfce83a766f128c27cc29',
		ETH_WALLET='0xaA9C0783C8c1af410d2C0558EBCd40B75e58a3F9',
		CONTRACT_ADDR='0x665Ff7628C18c5BcAFC464723dF1383418e30AbD',
		SEND_FILE_MAX_AGE_DEFAULT=1
	)
	
	try:
		os.makedirs(app.instance_path)
	except OSError:
		pass

	db.init_app(app)
	tickets.init_contract(app)
	
	app.register_blueprint(users.bp)
	app.register_blueprint(tickets.bp)
	app.add_url_rule('/', endpoint='index')
	
	return app
	