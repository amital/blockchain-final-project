abi = """[
	{
		"constant": false,
		"inputs": [
			{
				"name": "account_id",
				"type": "address"
			},
			{
				"name": "new_owner",
				"type": "address"
			},
			{
				"name": "amount",
				"type": "uint32"
			}
		],
		"name": "setTransferPending",
		"outputs": [],
		"payable": false,
		"stateMutability": "nonpayable",
		"type": "function"
	},
	{
		"constant": true,
		"inputs": [
			{
				"name": "account_id",
				"type": "address"
			}
		],
		"name": "getAccountTickets",
		"outputs": [
			{
				"name": "",
				"type": "uint32"
			}
		],
		"payable": false,
		"stateMutability": "view",
		"type": "function"
	},
	{
		"constant": false,
		"inputs": [
			{
				"name": "account_id",
				"type": "address"
			},
			{
				"name": "amount",
				"type": "uint32"
			}
		],
		"name": "buyTicket",
		"outputs": [],
		"payable": false,
		"stateMutability": "nonpayable",
		"type": "function"
	},
	{
		"constant": false,
		"inputs": [
			{
				"name": "account_id",
				"type": "address"
			},
			{
				"name": "new_owner",
				"type": "address"
			}
		],
		"name": "transferTicket",
		"outputs": [],
		"payable": false,
		"stateMutability": "nonpayable",
		"type": "function"
	},
	{
		"constant": false,
		"inputs": [
			{
				"name": "account_id",
				"type": "address"
			},
			{
				"name": "new_owner",
				"type": "address"
			}
		],
		"name": "unsetTransferPending",
		"outputs": [],
		"payable": false,
		"stateMutability": "nonpayable",
		"type": "function"
	},
	{
		"constant": true,
		"inputs": [],
		"name": "getTotalAvailable",
		"outputs": [
			{
				"name": "",
				"type": "uint32"
			}
		],
		"payable": false,
		"stateMutability": "view",
		"type": "function"
	},
	{
		"constant": false,
		"inputs": [
			{
				"name": "account_id",
				"type": "address"
			}
		],
		"name": "unsetPending",
		"outputs": [],
		"payable": false,
		"stateMutability": "nonpayable",
		"type": "function"
	},
	{
		"constant": true,
		"inputs": [
			{
				"name": "account_id",
				"type": "address"
			}
		],
		"name": "getPending",
		"outputs": [
			{
				"name": "",
				"type": "uint32"
			}
		],
		"payable": false,
		"stateMutability": "view",
		"type": "function"
	},
	{
		"constant": false,
		"inputs": [
			{
				"name": "account_id",
				"type": "address"
			},
			{
				"name": "amount",
				"type": "uint32"
			}
		],
		"name": "setPending",
		"outputs": [
			{
				"name": "",
				"type": "bool"
			}
		],
		"payable": false,
		"stateMutability": "nonpayable",
		"type": "function"
	},
	{
		"inputs": [
			{
				"name": "number_of_tickets",
				"type": "uint32"
			}
		],
		"payable": false,
		"stateMutability": "nonpayable",
		"type": "constructor"
	}
]"""