## Aman Mital
# blockchain-final-project

Class project for the CSE 40222 Blockchain class at the University of Notre Dame, Spring 2019.

## Setup

 - (Recommended) Create/active a python virtual environment (tested with Python 3.6.7)
 - Install dependencies `pip install -r requirements.txt`
 - The project assumes that you have a blockchain instance running on localhost port 8545. If this is not the case, you can modify this in
 `crypticket/tickets.py`
 - After compiling/deploying the contract `contracts/CrypTicket.sol`, put the details
(contract address, owners wallet address/private key)
 - If on a Linux system, can run the script `start-server.sh`
 - Otherwise, set the environment variable `FLASK_APP` to 'crypticket' and then run `flask run`